from fastapi import FastAPI
from app import router as get_form

app = FastAPI()

app.include_router(get_form, prefix="/get_form", tags=["get_form"])

if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000, reload=True)

import requests

url = 'http://localhost:8000/get_form'
data = {'user_name': 'erlan', 'order_date': '2023-03-11', 'phone': '+7 123 456 78 90', 'email': 'text@mail.com'}
response = requests.post(url, json=data)
print(response.json())  # TestForm

data = {'order_date': '2023-03-11', 'user_name': 'Erlan'}
response = requests.post(url, json=data)
print(response.json())  # OrderForm

data = {'phone': '+7 123 456 78 90', 'email': 'text@mail.com'}
response = requests.post(url, json=data)
print(response.json())  # MyForm


data = {'phone2': '+7 123 456 78 90', 'email': '2023-03-11', }
response = requests.post(url, json=data)
print(response.json())
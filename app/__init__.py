from fastapi import APIRouter
from app.get_form import router as form_data

router = APIRouter()
router.include_router(form_data, prefix="")

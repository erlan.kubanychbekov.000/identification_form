from fastapi import APIRouter
from typing import Dict
from .validation import validate_field_type, determine_field_type
from app.db import init_db

db = init_db()
router = APIRouter()


@router.post('/')
async def get_form_data(data: Dict):
    templates = db.all()

    matching_templates = []

    for template in templates:
        matching_fields = dict(filter(lambda item: item[1] in data.keys(), template.items()))
        if matching_fields and (len(template) - 1) == len(matching_fields):
            if all(validate_field_type(data[v], template[k]) for k, v in matching_fields.items()):
                matching_templates.append(template)

    if matching_templates:
        return {"template_name": max(matching_templates, key=len)["name"]}

    field_types = {field: determine_field_type(data[field]) for field in data}
    return field_types

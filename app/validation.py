import re
from datetime import datetime


def validate_date(date_str):
    try:
        datetime.strptime(date_str, '%Y-%m-%d')
        return True
    except ValueError:
        return False


def validate_phone(phone_str):
    phone_pattern = re.compile(r'^\+7 \d{3} \d{3} \d{2} \d{2}$')
    return bool(re.match(phone_pattern, phone_str))


def validate_email(email_str):
    email_pattern = re.compile(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
    return bool(re.match(email_pattern, email_str))


def determine_field_type(value):
    if validate_date(value):
        return "date"
    elif validate_phone(value):
        return "phone"
    elif validate_email(value):
        return "email"
    else:
        return "text"


def validate_field_type(value, expected_type):
    actual_type = determine_field_type(value)
    if actual_type == 'text':
        return not any(expected_type.count(type) for type in ['date', 'phone', 'email'])
    return expected_type.count(actual_type)

fastapi==0.68.0
uvicorn==0.15.0
tinydb==4.5.1
pydantic==1.8.2